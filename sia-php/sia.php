<?php
/**
 * @copyright Copyright (c) 2017,2021 Nebulous
 *
 * @author Johnathan Howell <me@johnathanhowell.com>
 * @author Florian Wiessner <f.wiessner@smart-weblications.de>
 *
 * @license MIT
 * */
namespace Sia;

require_once __DIR__ . '/Requests/library/Requests.php';
\Requests::register_autoloader();

define('SIA_AGENT', 'Sia-Agent Nextcloud');

class Client
{

    private $apiaddr;
    private $apipass;
    private $siaprefix;

    private function apiGet($route, $extras = array() )
    {
        $url = $this->apiaddr . $route;
        $options = array(
            'auth' => array(
                '',
                $this->apipass
            )
        );

        $res = \Requests::get($url, array(
            'User-Agent' => SIA_AGENT
        ), $options);
        if (isset($extras['raw']) && $extras['raw'] == true ) {
            return $res;
        }

        if ($res->status_code < 200 || $res->status_code > 299 || ! $res->success) {
             throw new \Exception(json_decode($res->body)->message);
        }

        return json_decode($res->body);
    }

    private function apiPost($route, $post_fields = NULL)
    {
        $extras = array(
            'auth' => array(
                '',
                $this->apipass
            )
        );
 

        $headers = array(
            'Content-Type: application/x-www-form-urlencoded',
            'User-Agent: ' . SIA_AGENT
        );

        if (preg_match("/\?/", $route)) {
            list ($target, $source) = preg_split("/\?/", $route);
        } else {
            $target = $route;
            $source = "";
        }
        $ch = curl_init($this->apiaddr . $target);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLOPT_USERPWD, ':' . $this->apipass);
        curl_setopt($ch, CURLOPT_TIMEOUT, 3600);
        curl_setopt($ch, CURLOPT_POST, 1);
        if (strlen($source) > 0) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $source);
        }
        if (isset($post_fields)) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post_fields);
        }
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        $return = curl_exec($ch);

        if (curl_errno($ch)) {
            throw new \Exception(curl_error($ch));
        }
        curl_close($ch);
        return json_decode($return);
    }

    public function __construct($apiaddr, $apipass)
    {
        if (! is_string($apiaddr)) {
            throw new \InvalidArgumentException('api addr must be a string');
        }
        if (! is_string($apipass)) {
            throw new \InvalidArgumentException('api pass must be a string');
        }
        $this->apiaddr = 'http://' . $apiaddr;
        $this->apipass = $apipass;
    }

    // Daemon API
    // version returns a string representation of the current Sia daemon version.
    public function version()
    {
        return $this->apiGet('/daemon/version')->version;
    }

    // Wallet API
    // wallet returns the wallet object
    public function wallet()
    {
        return $this->apiGet('/wallet');
    }

    // Renter API
    // renterSettings returns the renter settings
    public function renterSettings()
    {
        return $this->apiGet('/renter');
    }

    // renterFiles returns the files in the renter
    public function renterFiles()
    {
        return $this->apiGet('/renter/files')->files;
    }

    // renterContracts returns the contracts being used by the renter
    public function renterContracts()
    {
        return $this->apiGet('/renter/contracts')->contracts;
    }

    // download downloads the file at $siapath to $dest.
    public function download($siapath, $dest)
    {
        $this->apiGet('/renter/downloadasync/' . $siapath . '?destination=' . $dest);
    }
    
    public function download_stream($siapath, $range = '0-2048' ) {

        $headers = array(
            'User-Agent: ' . SIA_AGENT,
            'Range: bytes='.$range
        );
        $url = $this->apiaddr . '/renter/stream/'.$this->prepare_path($siapath) ; 
        
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_USERPWD, ':' . $this->apipass);
        curl_setopt($ch, CURLOPT_TIMEOUT, 3600);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        $return = curl_exec($ch);

        if (curl_errno($ch)) {
            throw new \Exception(curl_error($ch));
        }
        curl_close($ch);
        return $return;
    }

    public function downloads()
    {
        return $this->apiGet('/renter/downloads')->downloads;
    }

    public function upload($siapath, $src)
    {
        $this->apiPost('/renter/upload/' . $this->prepare_path($siapath) . '?source=' . $src);
    }

    public function delete($siapath)
    {
        $this->apiPost('/renter/delete/' . $this->prepare_path($siapath));
    }

    public function rmdir($siapath)
    {
        $this->apiPost('/renter/dir/' . $this->prepare_path($siapath), 'action=delete');
    }

    public function mkdir($siapath)
    {
        $this->apiPost('/renter/dir/' . $this->prepare_path($siapath), 'action=create');
    }

    public function rename($siapath, $newsiapath)
    {
        $this->apiPost('/renter/rename/' . $this->prepare_path($siapath) , 'newsiapath=' . $this->prepare_path($newsiapath));
    }

    public function rename_dir($siapath, $newsiapath)
    {
        $this->apiPost('/renter/dir/' . $this->prepare_path($siapath) , 'action=rename&newsiapath='.$this->prepare_path($newsiapath));
    }

    public function lsdir($siapath)
    {
        return $this->apiGet('/renter/dir/' . $siapath);
    }

    public function is_dir($siapath)
    {
        $res = $this->apiGet('/renter/dir/' . $siapath, array('raw' => true) );
        if ($res->status_code == 500) {
            return false;
        }
        if (($res->status_code >= 200 && $res->status_code <= 299)) {
            return true;
        }
        return false;
    }

    public function is_file($siapath)
    {
        $res = $this->apiGet('/renter/file/' . $siapath, array('raw' => true) );
        if ($res->status_code == 500) {
            return false;
        }
        if (($res->status_code >= 200 && $res->status_code <= 299)) {
            return true;
        }
        return false;
    }

    public function get_file_size($siapath)
    {
        return $this->apiGet('/renter/file/' . $siapath)->file->filesize;
    }

    public function get_file_mtime($siapath)
    {
        $mtime = $this->apiGet('/renter/file/' . $siapath);
        $time = $mtime->file->changetime;
        return date('U', strtotime($time));
        ;
    }

    public function get_dir_mtime($siapath)
    {
        $mtime = $this->apiGet('/renter/dir/' . $siapath);
        $time = $mtime->directories[0]->mostrecentmodtime;
        return date('U', strtotime($time));
        ;
    }

    private function prepare_path($siapath) {
        // convert space
        $siapath = str_replace(" ","%20",$siapath);
        
        return $siapath;
    }
}

