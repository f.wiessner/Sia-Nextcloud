<?php
/**
 * @copyright Copyright (c) 2021 Smart Weblications GmbH
 *
 * @author Florian Wiessner <f.wiessner@smart-weblications.de>
 *
 * @license MIT
 * */


class siastream
{

    var $position;
    var $path;

    function __construct() {
    }

    function stream_open($path, $mode, $options, &$opened_path)
    {
        
        $url = $url = parse_url($path);
        $data = base64_decode($url['host']);
        
        list($apiaddr,$apipass,$siaprefix,$siapath)  = preg_split("/\|/", $data);

        $this->client = new \Sia\Client($apiaddr,$apipass);
        $this->siaprefixpath = $siaprefix ; 
        
        $this->siapath = $siapath;
        $this->position = 0;
        
        $this->eof = $this->client->get_file_size($this->siaprefixpath.$siapath) ;
        return true;
    }
    
    
    function stream_read($count)
    {
        $start = $this->position; 
        if ($this->position > $this->eof) {
            return '';
        }

        if ($this->position + $count >= $this->eof) {
            $end = $this->eof;
        } else {
            $end = $start + $count; 
        }
        $ret =  $this->client->download_stream($this->siaprefixpath.$this->siapath, $start."-".$end ); 
        $this->position += strlen($ret);
        return $ret;
    }

    function stream_eof()
    {
        return $this->position >= $this->eof;
    }

    function stream_seek($offset, $whence)
    {
        switch ($whence) {
            case SEEK_SET:
                if ($offset < $this->eof && $offset >= 0) {
                     $this->position = $offset;
                     return true;
                } else {
                     return false;
                }
                break;

            case SEEK_CUR:
                if ($offset >= 0) {
                     $this->position += $offset;
                     return true;
                } else {
                     return false;
                }
                break;

            case SEEK_END:
                if ($this->eof + $offset >= 0) {
                     $this->position = $this->eof + $offset;
                     return true;
                } else {
                     return false;
                }
                break;

            default:
                return false;
        }
    }

    function stream_metadata($path, $option, $var) 
    {
        if($option == STREAM_META_TOUCH) {
            return true;
        }
        return false;
    }

    function stream_tell() {
        return $this->position;
    }
    
    function stream_stat() {

     $ret = array(
       'size' => $this->eof, 
       'blksize' =>  32768, 
       'blocks' => ceil(($this->eof / 32768)), 
       'atime' => date('U'),
       'mtime' => date('U'), 
       'ctime' => date('U'), 
       'uid' => 33, 
       'gid' => 33,
       'dev' => 771, 
       'ino' => 1, 
       'rdev' => 0, 
       'nlink' => 1
      );   
        return $ret;
    
    }


    function stream_close() {
        return true;
    }
    
}
