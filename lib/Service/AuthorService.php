<?php
namespace OCA\Files_External_Sia\Service;

use \OCP\ILogger;

class AuthorService {

    private $logger;
    private $appName;

    public function __construct(ILogger $logger, $appName){
        $this->logger = $logger;
        $this->appName = $appName;
    }

    public function log($message) {
        $this->logger->error($message, ['app' => $this->appName]);
    }

}