<?php
/**
 * @copyright Copyright (c) 2016, Nebulous, Inc.
 *
 * @author Johnathan Howell <me@johnathanhowell.com>
 *
 * @copyright Copyright (c) 2021, Smart Weblications GmbH
 *
 * @author Florian Wiessner <f.wiessner@smart-weblications.de>
 *
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License, version 3,
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 */
namespace OCA\Files_External_Sia\Storage;

use OCP\ILogger;
set_include_path(get_include_path() . PATH_SEPARATOR . \OC_App::getAppPath('files_external_sia') . '/sia-php');

require_once 'sia.php';
require_once 'sia_stream.php'; 

stream_wrapper_register("sia", "siastream") or die("Failed to register protocol");                

use OCP\Files\ForbiddenException;
use Icewind\Streams\IteratorDirectory;
use Icewind\Streams\CallbackWrapper;

class Sia extends \OC\Files\Storage\Common
{

    private $client;
    private $apiaddr;
    private $datadir;
    


    public function __construct($arguments)
    {
        if (! isset($arguments['apiaddr']) || ! is_string($arguments['apiaddr'])) {
            throw new \InvalidArgumentException('no api address set for Sia');
        }
        if (! isset($arguments['apipass']) || ! is_string($arguments['apipass'])) {
            throw new \InvalidArgumentException('no api pass set for Sia');
        }

       
        $this->client = new \Sia\Client($arguments['apiaddr'], $arguments['apipass'] );
        $this->apiaddr = $arguments['apiaddr'];
        $this->apipass = $arguments['apipass'];
        $this->datadir = realpath($arguments['datadir']);

        
        if (isset($arguments['siaprefixpath'])) {
        if (preg_match("/^\//",$arguments["siaprefixpath"]) || preg_match("/(.*)\/$/",$arguments["siaprefixpath"])) {
            throw new \InvalidArgumentException('sia prefix must not begin nor end with a slash (/)!');        
        }        


            $this->siaprefixpath = $arguments['siaprefixpath']; // make a prefix for all nextcloud data, e.g. nextcloud
        }
        
        if (isset($arguments['jailusers'])) {
            $this->jailusers = $arguments['jailusers']; // if true, we add the current user to the prefix path so every user is isolated on external storage with sia
            if ($this->jailusers == "true") {
                $user_id = \OC_User::getUser();
                $this->siaprefixpath .= '/' . $user_id . '/';
            }
        }
        
    }

    // parsePaths takes an array of siafiles and a path and returns an array of
    // siafiles that contain $path at the beginning of their siapath.
    private function parsePaths($siafiles, $path)
    {
        $ret = array();
        foreach ($siafiles as $siafile) {
            if ($path === "/") {
                array_push($ret, $siafile);
            } else if (strpos($siafile, $path) === 0) {
                $components = explode($path, $siafile, 2);
                array_push($ret, $components[count($components) - 1]);
            }
        }

        return $ret;
    }

    // ls takes an array of siafiles and a path and returns an array of names of
    // directories or files in that path.
    private function ls($path)
    {
        $ret = array();

        $result = $this->client->lsdir($this->siaprefixpath . $path);

        if (isset($result->directories)) {
        foreach ($result->directories as $dir) {
            if ($dir->siapath != $this->siaprefixpath) {
                array_push($ret, $dir->siapath);
            }
        }
        }

        if (isset($result->files)) {
        foreach ($result->files as $file) {
            array_push($ret, $file->siapath);
        }
        }
        
        $ret = preg_replace("|" . $this->siaprefixpath . $path ."/"."|", "", $ret);
        $ret = preg_replace("|" . $this->siaprefixpath . $path ."|", "", $ret);        

        array_splice($ret, array_search($this->siaprefixpath, $ret), 1); // remove the actual path from results

        return array_values($ret);
    }

    // localFile takes a $siapath and returns the location of the file on disk.
    // Temporary files ('.octransfer*') will have the same hash as their final
    // forms.
    private function localFile($siapath)
    {
        $cleanpath = $siapath;
        if (strpos($siapath, '.ocTransferId') !== false) {
            $cleanpath = explode('.ocTransferId', $siapath)[0];
        }
        return $this->datadir . '/' . hash('sha256', $cleanpath);
    }

    public function __destruct()
    {}

    public function getId()
    {
        return 'sia::' . $this->apiaddr;
    }

    public function mkdir($path)
    {
        $this->client->mkdir($this->siaprefixpath . $path);
        return true;
    }

    public function rmdir($path)
    {
        if ($path == "") {
            return false;
        }
        if ($path == $this->siaprefixpath) {
            return false;
        }
        $this->client->rmdir($this->siaprefixpath . $path);
        return true;
    }

    public function opendir($path)
    {
        $result = $this->ls($path);
        return IteratorDirectory::wrap($result);
    }

    public function lsdir($path)
    {
        if (preg_match("/^\//", $path)) {
            $path = substr($path, 1);
        }
        $res = $this->client->lsdir($this->siaprefixpath . $path);
        return $res;
    }

    // test the node's upload capability by verifying that the node has some
    // contracts and the renter data directory is writeable.
    public function test()
    {
        try {
            $contracts = $this->client->renterContracts();
            if (count($contracts) === 0) {
                return false;
            }
            if (! is_writeable($this->datadir)) {
                return false;
            }
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    // directorySize returns the aggregate filesize of every file in the directory supplied by $path.
    private function directorySize($path)
    {
        $sz = 0;
        $data = $this->client->lsdir($this->siaprefixpath . $path);
        if (isset($data->files)) {
        $files = $data->files;
               
        foreach ($files as $entry) {
            $sz += $entry->filesize;
        }
        }
        return $sz;
    }

    public function stat($path)
    {
        clearstatcache();
        $ret = array();
        $ret['size'] = 0 ;
        
        if ($this->is_dir($this->siaprefixpath . $path)) {
            $ret['size'] = $this->directorySize($path);
            return $ret;
        }

        $files = $this->client->renterFiles();
        foreach ($files as $file) {
            if (strpos($file->siapath, $this->siaprefixpath . $path) !== false) {
                $ret['size'] = $file->filesize;
                return $ret;
            }
        }

        return false;
    }

    public function filetype($path)
    {
        if (preg_match("/^\//", $path)) {
            $path = substr($path, 1);
        }
        if ($this->is_dir($this->siaprefixpath . $path)) {
            return 'dir';
        }
        return 'file';
    }

    public function is_dir($path)
    {
     
        if (preg_match("/^\//", $path)) {
            $path = substr($path, 1);
        }
    
        if (preg_match("|^".$this->siaprefixpath."|",$path)) {
            $result = $this->client->is_dir($path);
        } else { 
            $result = $this->client->is_dir($this->siaprefixpath . $path);
        }
 
        if ($result === true) {
            return true;
        }

        return false;
    }

    public function is_file($path)
    {
        if (preg_match("/^\//", $path)) {
            $path = substr($path, 1);
        }

        $result = $this->client->is_file($path);
        if ($result === true) {
            return true;
        }

        return false;
    }

    // filesize returns the file size in bytes of the file at $path.
    public function filesize($path)
    {
        $path = preg_replace("/^\//", "", $path);
        $sz = 0;

        if ($this->is_dir($this->siaprefixpath . $path)) {
            return $this->directorySize($path);
        }

        $file_size = $this->client->get_file_size($this->siaprefixpath . $path);

        if (! $file_size) {
            return false;
        }
        return $file_size;
    }

    public function isDeletable($path)
    {
        if ($path === '') {
            return false;
        }

        if ($this->file_exists($path)) {
            return true;
        }

        return false;
    }

    // file_exists checks for the existence of the file at $path on the Sia node.
    public function file_exists($path)
    {
        return $this->is_dir($this->siaprefixpath .$path) || $this->is_file($this->siaprefixpath .$path);
    }

    public function isCreatable($path)
    {
        return true;
    }

    public function filemtime($path)
    {
  
        $path = preg_replace("/^\//", "", $path);
        
        if ($this->is_dir($this->siaprefixpath . $path)) {
            $mtime = $this->client->get_dir_mtime($this->siaprefixpath . $path);
            return $mtime;
        }
        $mtime = $this->client->get_file_mtime($this->siaprefixpath . $path);
        return $mtime;
    }

    public function touch($path, $mtime = null)
    {
        return true;
    }

    public function unlink($path)
    {
        try {
            $this->client->delete($this->siaprefixpath . $path);
            if (file_exists($this->localFile($this->siaprefixpath . $path))) {
                unlink($this->localFile($this->siaprefixpath . $path));
            }
        } catch (\Exception $e) {
            return false;
        }
        return true;
    }

    public function getPermissions($path)
    {
        return \OCP\Constants::PERMISSION_ALL;
    }

    public function rename($path1, $path2)
    {
        if ($this->is_dir($path1)) {
            if ($this->is_dir($path2)) {
                return false;
            }
            try {
                $this->client->rename_dir($this->siaprefixpath . $path1, $this->siaprefixpath . $path2);            
            } catch (\Exception $e) {
                return false;
            }
        }
        try {
                $this->client->rename($this->siaprefixpath . $path1, $this->siaprefixpath . $path2);            
        } catch (\Exception $e) {
            return false;
        }
        return true;
    }

    // isFileInDownloads returns a bool true if a file exists in the Sia node's
    // downloads, otherwise it returns false.
    private function isFileInDownloads($siapath)
    {
        $exists = false;

        $downloads = $this->client->downloads();
        foreach ($downloads as $download) {
            if ($download->siapath == $siapath && $download->error == "") {
                $exists = true;
            }
        }

        return $exists;
    }

    // fileAtSiapath returns the file object at the requested siapath, if it
    // exists.
    private function fileAtSiapath($siapath)
    {
        $files = $this->client->renterFiles();
        if (is_array($files)) {
            if (count($files) > 0) {
                foreach ($files as $file) {
                    if ($file->siapath == $this->siaprefixpath . $siapath) {
                        return $file;
                    }
                }
            }
        }
        return false;
    }

    // nodeHasFile returns true if this node has a valid copy of the supplied
    // siapath.
    private function nodeHasFile($siapath)
    {
        $file = $this->fileAtSiapath($siapath);
        if (! $file) {
            return false;
        }

        if (file_exists($this->localFile($siapath))) {
            return filesize($this->localFile($siapath)) == $file->filesize;
        }

        return false;
    }

    public function fopen($path, $mode)
    {
        switch ($mode) {
            case 'r':
            case 'rb':
                // if this file is on disk, just return a handle to it


                // TODO: this is working, but slowly with only 8kb/sek because of DAV only reading 8k chunks...
                //	 need to find a way to get bigger chunks of data to speed up the direct streaming

                // return fopen("sia://" . base64_encode($this->apiaddr."|".$this->apipass."|".$this->siaprefixpath."|".$path) , "r");

                if ($this->nodeHasFile($path)) {
                    return fopen($this->localFile($path), 'r');
                }

                // if this file hasn't been downloaded already, download it and return
                // a file informing the user what is happening.
                if (! $this->isFileInDownloads($path)) {
                    $this->client->download($this->siaprefixpath . $path, $this->localFile($path));
                }

                return fopen('data://text/plain,' . $path . ' is currently being downloaded from Sia.', $mode);
            case 'w':
            case 'wb':
            case 'a':
            case 'ab':
            case 'r+':
            case 'w+':
            case 'wb+':
            case 'a+':
            case 'x':
            case 'x+':
            case 'c':
            case 'c+':
                $localfile = $this->localFile($path);
                $handle = fopen($localfile, $mode);
                return CallbackWrapper::wrap($handle, null, null, function () use ($path, $localfile) {
                    $this->client->upload($this->siaprefixpath . $path, $localfile);
                });
        }
    }

    public function hasUpdated($path, $time)
    {
        return true;
    }

    public function isReadable($path)
    {
        return true;
    }

}
