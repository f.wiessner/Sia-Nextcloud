# Sia-Nextcloud

This is the [Nextcloud](https://nextcloud.com/) Sia storage backend implementation. It adds a 'Sia' option to the External Files Nextcloud app, allowing users to upload and download from a Sia node through the Nextcloud UI.

## Requirements

This requires some  parameters to work:

1. API Address: This API Address must point to a reachable, synchronized Sia node that has money set aside in an allowance.

example: 127.0.0.1:9980

2. API Password: can be found in ~/.sia/apipassword

example: 342342234234b234babab3babbbcbc3bb3be

3. Renter-Data: directory for caching files

example: /var/lib/nextcloud/renterdata

4. Prefix Path: a directory within sia where to put the files

example: nextcloud

5. Jail Users: set to true to enable jailing of users within the sia prefix 

example: true - anything else will disable this. "true" as string without quotes

if set to true, this will create files as follows in the sia namespace:

/nextcloud/username/files



my Sia wallet:
SC: 1d9855ca88a07af2811fe6f277b062a6732cda68911ea34e43e8408bf4be376026a5b8c48218

